import {Fragment, useEffect, useState} from 'react';
import CourseCard from '../components/CourseCard';
import coursesData from '../data/coursesData';

export default function Courses(){
	console.log(coursesData);
	console.log(coursesData[0]);

	const[courses, setCourses] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);
				setCourses(data.map(course => {
					return (
						<CourseCard key={course.id} courseProp={course} />
						)
					})
				)
			})
	},[])



	return (
		<Fragment>
			{courses}
		</Fragment>
	)
}