import {Row,Col,Card} from 'react-bootstrap';
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

export default function	CourseCard({courseProp}){
	const {_id,name,description,price} = courseProp;

	// const [count, setCount] = useState(0);

	// const [seat, seatCount] = useState(30);

	// function enroll(){
	// 	setCount(count + 1);
	// 	seatCount(seat - 1)
	// 	if(seat === 0){
	// 		setCount(count);
	// 		seatCount(seat);
	// 		alert('No more seats');
	// 	}
	// }
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={12}>
				<Card className = "cardHighlight ml-auto">
				  <Card.Body>
				    <Card.Title>{name}</Card.Title>
				    <Card.Text>
				     	<h6 className="mb-0">Description:</h6>
				     	<p className="mb-3">{description}</p>
				     	<h6 className="mb-0">Price:</h6>
				     	<p className="mb-3">PhP {price}</p>
				     	<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}

CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.string.isRequired
	})
}